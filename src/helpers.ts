export function objectify(clazz: Record<string, any>) {
    const prototype = Object.getPrototypeOf(clazz);
    const methods = Object.getOwnPropertyNames(prototype).slice(1);
    const object: Record<string, any> = {};
    for (const method of methods) {
        const handle = clazz[method];
        object[method] = (obj: Record<string, any>) => {
            handle.bind(clazz).call(clazz, obj);
        };
    }
    return object;
}

export function loadBundled<T>(name: string) {
    // @ts-ignore
    return require.main.require(name) as T;
}

export function loadSrc<T>(name: string) {
    // @ts-ignore
    return require.main.require(`./src${name}`) as T;
}
