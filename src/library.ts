import type { Plugin, Settings, Topics, ActionPostSaveParams, DiscordWebhook, 
    Users, Embed, Winston, NConf, ActionPostEditParams, 
    ActionPostReport} from "./types";
import { loadSrc, loadBundled, objectify } from "./helpers";

const winston = loadBundled<Winston>("winston");
const nconf = loadBundled<NConf>("nconf");

const settings = loadSrc<Settings>("/meta/settings");
const topics = loadSrc<Topics>("/topics/");
const users = loadSrc<Users>("/user/");

const EmbedColors = {
    Red: 15548997,
    Blue: 3447003,
    Purple: 10181046
};

const PluginEnvPrefix = "NODEBB_DISCORD";
const WebhookEnv = `${PluginEnvPrefix}_WEBHOOK`;

class DiscordPlugin implements Plugin {    
    private webhook?: string;
    private domain: string = "http://localhost";

    private env(name: string) {
        return process.env[WebhookEnv] as string;
    }

    async init() {
        this.webhook = this.env(WebhookEnv);
        this.domain = nconf.get("url") as string;
        winston.info(`[discord] Forwarding posts to ${this.webhook}`);
    }

    private async dispatch(data: DiscordWebhook) {
        if (!this.webhook) {
            return winston.error(`[discord] Webhook url not defined in environment (${WebhookEnv})!`);
        }
        await fetch(this.webhook, {
            body: JSON.stringify(data),
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            }
        });
    }

    async onPostSave({ post }: ActionPostSaveParams) {
        const topic = await topics.getTopicData(post.tid);
        const user = await users.getUserData(post.uid);
        
        const embed: Embed = {
            title: topic.title,
            author: {
                name: `New post by ${user.displayname}`,
                url: `${this.domain}/user/${user.userslug}`
            },
            url: `${this.domain}/topic/${topic.slug}/${topic.postcount}`,
            description: post.content,
            color: EmbedColors.Red
        };

        if (topic.postcount > 1) {
            const originalPoster = await users.getUserData(topic.uid);
            embed.color = EmbedColors.Blue;
            embed.title = `Re: ${topic.title}`;
            embed.author!.name = `New reply by ${user.displayname}`;
            embed.footer = {
                text: `Originally posted by ${originalPoster.displayname} (${originalPoster.userslug})`
            };
        }

        await this.dispatch({            
            embeds: [embed]
        });
    }

    async onPostEdit({ post }: ActionPostEditParams) {
        const topic = await topics.getTopicData(post.tid);
        const user = await users.getUserData(post.uid);

        const embed: Embed = {
            title: topic.title,
            author: {
                name: `Post edited by ${user.displayname}`,
                url: `${this.domain}/user/${user.userslug}`
            },
            url: `${this.domain}/topic/${topic.slug}/${topic.postcount}`,
            description: post.content,
            color: EmbedColors.Purple
        };

        await this.dispatch({            
            embeds: [embed]
        });
    }
    
}

module.exports = objectify(new DiscordPlugin());
