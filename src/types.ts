export type Plugin = {
    init: () => Promise<void>,

    /* Hooks */
    onPostSave?: (params: ActionPostSaveParams) => Promise<void>

    onPostEdit?: (params: ActionPostEditParams) => Promise<void>

    onPostReport?: (params: ActionPostReport) => Promise<void>
};

/* Objects */

export type Winston = {
    info: (text: string) => void;
    error: (text: string) => void;
};

export type NConf = {
    get: (key: string) => string | undefined;
};

export type Settings = {
    get: (hash: string) => Promise<Record<string, any>>
};

export type User = {
    uid: number,
    username: string,
    userslug: string,
    displayname: string
};

export type Users = {
    getUserData: (uid: number) => Promise<User>
};

export type Topic = {
    slug: string,
    title: string,
    uid: number,
    postcount: number
};

export type Topics = {
    getTopicData: (tid: string) => Promise<Topic>
};

export type Request = {
    uid: number,
    url: string,
    path: string
};

export type Caller = {
    uid: number,
    req: Request
};

export type Post = {
    /**
     * Unique post id
     */
    pid: number,
    /**
     * User id that created the post
     */
    uid: number,
    /**
     * Topic id
     */
    tid: string,
    /**
     * String contents of the post
     */
    content: string,
    /**
     * Timestamp of the post
     */
    timestamp: number,
    ip?: string
};

export type EditedPost = {
    changed: boolean,
    oldContent: string,
    newContent: string
} & Post;

/* Hook params */

export type ActionPostReport = {
    flagId: number,
    type: string,
    id: number,
    uid: number,
    timestamp: number,
    reason: string
};

export type ActionPostSaveParams = {
    post: Post,
    caller: Caller
};

export type ActionPostEditParams = {
    post: EditedPost
};

/* Discord types */

export type Embed = {
    title?: string,
    url?: string,
    author?: {
        name: string,
        url?: string
    },
    description: string,
    color?: number,
    footer?: {
        text: string
    },
    timestamp?: string
};

export type DiscordWebhook = {
    username?: string,
    content?: string,
    embeds?: Embed[]
};
